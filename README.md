# Taller02
Usted es dueño de una tienda de herramientas y necesita mantener un inventario que le pueda decir cuáles herramientas tiene, cuantas tiene y el costo de cada una. Escriba un programa que a partir del archivo “herram.txt” suba los datos al programa relacionados con algunas herramientas, que le permita listar todas sus herramientas, que le permita eliminar un registro de una herramienta que ya no tiene. El número de identificación de cada herramienta debe ser su númerode registro. Tiene como Máximo 100 tipos diferentes de Herramientas. 

De igual manera usted desea generar un archivo “sal.txt” con las herramientas cuyo costototal sea superior a $100000, en el archivo debe  quedar  el  #  de  registro,  Nombre  de  la herramienta y el costoTotal, Ordenado ascendente mente por el #deregistro.

Cuando se inserta los datos de una herramienta de manera manual (digitada por el usuario) el archivo“herram.txt”, este debe quedar al final del archivo 

Utilice la siguiente información que tiene usted almacenada en su disco duro para subir al programa:

```text
17 Lijadora_Eléctrica 7 130000
3 Martillo 76 35000
56 Serrucho 21 38000
39 Podadora 3 230000
24 Sierra 18 310000
68 Destornillador 106 8000
83 Mazo 11 12000
77 Llave_Inglesa 34 15000
```
## Condiciones que debe cumplir el programa
Se debe realizar un menú con las diferentes opciones:
1. Capturar datos iniciales de las herramientas desde un archivo.
2. Listar las herramientas.
3. Insertar al final una herramienta de forma manual.
4. Eliminar una herramienta y actualiz ararchivo.
5. Gener ararchivo con herramientas por encima de 100000 como valor unitario–Ordenado.

