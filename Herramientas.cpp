#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

using namespace std;

struct herramienta{
    short registro;
    string nombre;
    short cantidad;
    int costo;
    int costoTotal;
};
 
int contar_lineas(fstream& file);
herramienta leer_archivo(string line, char delimiter);
void listar(herramienta inv[], int cantidad);
int nueva_herramienta(herramienta inv[], int cantidad);
int eliminar_herramienta(herramienta inv[], int cantidad);
int nuevo_arcihvo(herramienta inv[], int cantidad, herramienta caras[]);
void listar_caras(herramienta caras[], int cant_100);

int main(void){
	string file_name = "herram.txt";
    fstream file;
    file.open(file_name, ios::in);
    if (file.fail()) {
        cerr << "No se puede abrir el archivo." << endl;
    }    
    else {
        string line;
    	int cantidad = contar_lineas(file);
    	int cant_100;
    	herramienta inventario[100], caras[100];
        short opcion=0;
        
        while(opcion!=7){
            cout << "Bienvenido Manny, listo para ponerte a la obra? Selecciona la opción que desees: \n";
            cout << "1. Capturar datos iniciales de las herramientas desde un archivo. \n";
            cout << "2. Listar las herramientas. \n";
            cout << "3. Insertar al final una herramienta de forma manual. \n";
            cout << "4. Eliminar una herramienta y actualizar archivo. \n";
            cout << "5. Generar archivo con herramientas por encima de 100000 como valor unitario–Ordenado \n";
            cout << "6. Listar las herramientas de costo total mayor a 100000. \n";
            cout << "7. Salir. \n";
            cin >> opcion;
    
            if(opcion==1){
                const char DELIMITER = ' ';
                for (int i = 0; i < cantidad; i++){        
                    getline(file,line);        
                    inventario[i] = leer_archivo(line,DELIMITER);
                }file.close();
            }
            if(opcion==2){
                listar(inventario, cantidad);
            }
            if(opcion==3){
                cantidad = nueva_herramienta(inventario, cantidad);
            }
            if(opcion==4){
                cantidad = eliminar_herramienta(inventario, cantidad);
            }
            if(opcion==5){
                cant_100 = nuevo_arcihvo(inventario, cantidad, caras);
            }
            if(opcion==6){
                listar_caras(caras, cant_100);
            }
        }
    }
}
 
int contar_lineas(fstream& file){
    int cant = 0;
    string line;
    while (getline(file, line)){
        cant++;
    }
    file.clear();
    file.seekg(0);
    return cant;
}

herramienta leer_archivo(string line, char delimiter){
    string word; 
    herramienta herr_aux;
    stringstream str(line);

    getline(str, word, delimiter);
    herr_aux.registro = stoi(word);

    getline(str, word, delimiter);
    herr_aux.nombre = word;

    getline(str, word, delimiter);
    herr_aux.cantidad = stoi(word);

    getline(str, word, delimiter);
    herr_aux.costo = stoi(word);
    
    herr_aux.costoTotal = herr_aux.costo*herr_aux.cantidad;

    return herr_aux;

}

 
void listar(herramienta inv[], int cantidad){
	cout << "ID registro:  |         Nombre:         |   Cantidad:   |   Costo:   \n";
    for(int i=0; i<cantidad; i++){
        cout <<setw(7)<< inv[i].registro <<setw(30)<<inv[i].nombre <<setw(11)<<inv[i].cantidad <<setw(17)<<inv[i].costo<<"\n";
    } 
}

int nueva_herramienta(herramienta inv[], int cantidad){
    inv[cantidad].registro=0;
    int i=0;
    short idaux;
    cout<<"Ingrese el ID de la nueva herramienta, revise que no se repita: ";
    cin >> inv[cantidad].registro;
    cout<< "Ingrese el nombre de esta: \n";
    cin >> inv[cantidad].nombre;
    cout<< "Ingrese la cantidad: \n";
    cin >> inv[cantidad].cantidad;
    cout<< "Ingrese el costo unitario: \n";
    cin >> inv[cantidad].costo;
    inv[cantidad].costoTotal = inv[cantidad].cantidad*inv[cantidad].costo;
    
    string file_name = "herram.txt";
    fstream file;
    file.open(file_name, ios::app);
    if (file.fail()) {
        cerr << "No se puede abrir el archivo." << endl;
    }    
    else {
       file << "\n" << inv[cantidad].registro << " " << inv[cantidad].nombre << " "<< inv[cantidad].cantidad << " "<< inv[cantidad].costo;
    }
    cantidad++;
    return cantidad;
}

int eliminar_herramienta(herramienta inv[], int cantidad){
    short id;
    cout << "Digite el ID de la herramienta que desea eliminar:\n";
    cin >> id;
    for(int i=0; i<cantidad; i++){
        if(id==inv[i].registro){
            cantidad--;
            for(int j=i; j<cantidad; j++){
                inv[j].registro=inv[j+1].registro;
                inv[j].nombre=inv[j+1].nombre;
                inv[j].cantidad=inv[j+1].cantidad;
                inv[j].costo=inv[j+1].costo;
                inv[j].costoTotal=inv[j+1].costoTotal;
            }
        }
    }
    
    string file_name = "herram.txt";
    fstream file;
    file.open(file_name, ios::out);
    if (file.fail()) {
        cerr << "No se puede abrir el archivo." << endl;
    }    
    else {
        for(int i=0; i<cantidad; i++){
            file << inv[i].registro << " " << inv[i].nombre << " "<< inv[i].cantidad << " "<< inv[i].costo << "\n";
        }
    }
    return cantidad;
}

int nuevo_arcihvo(herramienta inv[], int cantidad, herramienta caras[]){
    int j=0;
    for(int i=0; i<cantidad; i++){
        if(inv[i].costoTotal>100000){
            caras[j].registro=inv[i].registro;
            caras[j].nombre=inv[i].nombre;
            caras[j].cantidad=inv[i].cantidad;
            caras[j].costo=inv[i].costo;
            caras[j].costoTotal=inv[i].costoTotal;
            j++;
        }
    }
    herramienta aux;
    short min;
    for(int i=0; i<j; i++){
        min=i;
        for(int k=i+1; k<j; k++){
            if(caras[k].registro<caras[min].registro){
                min=k;
            }
        }
        aux=caras[i];
        caras[i]=caras[min];
        caras[min]=aux;
    }
    
    
    string file_name = "mayor100.txt";
    fstream file;
    file.open(file_name, ios::out);
    if (file.fail()) {
        cerr << "No se puede abrir el archivo." << endl;
    }    
    else {
        for(int i=0; i<j; i++){
            file << caras[i].registro << " " << caras[i].nombre << " "<< caras[i].cantidad << " "<< caras[i].costo << "\n";
        }
    }
    return j;
}

void listar_caras(herramienta caras[], int cant_100){
	cout << "ID registro:  |         Nombre:         |   Cantidad:   |   Costo:   |    Costo total:   \n";
    for(int i=0; i<cant_100; i++){
        cout <<setw(7)<< caras[i].registro <<setw(30)<<caras[i].nombre <<setw(11)<<caras[i].cantidad <<setw(17)<<caras[i].costo<<setw(17)<<caras[i].costoTotal<<"\n";
    } 
}